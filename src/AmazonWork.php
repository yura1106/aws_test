<?php

namespace TestTask;


use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;
use Aws\Exception\AwsException;
use Aws\Rekognition\RekognitionClient;
use Aws\Ses\SesClient;

class AmazonWork
{
    /**
     * @var array
     */
    private $connection;
    /**
     * @var array
     */
    private $data;
    /**
     * @var string
     */
    private $bucketName;
    /**
     * @var array
     */
    private $env;

    /**
     * AmazonWork constructor.
     * @param array $data
     * @param array $env
     */
    public function __construct(array $data, array $env)
    {
        $this->connection = [
            'region' => 'us-east-1',
            'version' => '2006-03-01',
            'credentials' => [
                'key' => $env['ACCESS_KEY_ID'],
                'secret' => $env['SECRET_ACCESS_KEY']
            ],
        ];

        $this->data = $data;
        $this->bucketName = $env['BUCKET_NAME'];
        $this->env = $env;
    }

    /**
     * Get labels for files in bucket
     *
     * @param array $images
     * @return array
     */
    public function getLabels(array $images): array
    {
        $this->connection['version'] = 'latest';
        $rekognitionClient = new RekognitionClient($this->connection);

        $labelsList = [];

        try {
            print(json_encode([$this->connection, $this->bucketName]));
            foreach ($images as $object) {
                print($object['s3']['object']['key']);
                $result = $rekognitionClient->detectLabels([
                    'Image' => [
                        'S3Object' => [
                            'Bucket' => $this->bucketName,
                            'Name' => $object['s3']['object']['key']
                        ]
                    ],
                    'MaxLabels' => 5,
                    'MinConfidence' => 85
                ]);
                $labelsList[$object['s3']['object']['key']] = array_map(function($label) {
                    return $label['Name'];
                }, $result['Labels']);
            }

            return $labelsList;
        } catch (AwsException $e) {
            return ['message' => $e->getMessage() . '. ' . $e->getAwsErrorMessage()];
        }
    }

    /**
     * Send email via Amazon SES
     *
     * @param string $imageName
     * @return string
     */
    public function sendEmail(string $imageName): string
    {
        $this->connection['version'] = '2010-12-01';
        $SesClient = new SesClient($this->connection);
        $char_set = 'UTF-8';
        $plaintext_body = "Dog isn't found on " . $this->bucketName . " image /" . $imageName;
        $subject = 'Amazon SES and Rekognition test';

        try {
            $result = $SesClient->sendEmail([
                'Destination' => [
                    'ToAddresses' => [$this->env['RECIPIENT_EMAIL']],
                ],
                'ReplyToAddresses' => [$this->env['SENDER_EMAIL']],
                'Source' => $this->env['SENDER_EMAIL'],
                'Message' => [
                    'Body' => [
                        'Text' => [
                            'Charset' => $char_set,
                            'Data' => $plaintext_body,
                        ],
                    ],
                    'Subject' => [
                        'Charset' => $char_set,
                        'Data' => $subject,
                    ],
                ]
            ]);
            return $result['MessageId'];
        } catch (AwsException $e) {
            return $e->getMessage() . '. ' . $e->getAwsErrorMessage();
        }
    }

    /**
     * Save image and information to DynamoDb
     *
     * @param string $fileName
     * @param array $labels
     * @return string
     */
    public function saveToDB(string $fileName, array $labels): string
    {
        $this->connection['version'] = 'latest';
        $dynamoClient = new DynamoDbClient($this->connection);

        $marshaler = new Marshaler();

        $params = [
            'TableName' => 'images',
            'Item' => $marshaler->marshalItem([
                'image' => $fileName,
                'labels' => json_encode($labels)
            ])
        ];

        try {
            $dynamoClient->putItem($params);
            return 'Added';
        } catch (DynamoDbException $e) {
            return $e->getAwsErrorMessage();
        }
    }
}