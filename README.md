**For start**

`npm install -g serverless`

`composer install`

`npm install`

**Deploy the application**

`sls login`

`serverless config credentials --provider aws --key #key# --secret #secret#`

`sls deploy`

Copy .env.example to .env and set up variables.

SENDER_EMAIL must be verified in Amazon SES.

After deploying the application you should go to S3 bucket('app-#app#-assets') 
which was created and upload images to it.

For removing all data from Amazon

`sls remove`