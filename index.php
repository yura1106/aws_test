<?php declare(strict_types=1);
use TestTask\AmazonWork;

require __DIR__ . '/vendor/autoload.php';


return function ($event) {

    print(json_encode([$event, $_ENV]));

    if (!isset($event['Records'])) {
        return [];
    }

    $amazon = new AmazonWork($event, $_ENV);
    $sendResults = [];
    $dbResults = [];

    $labelsList = $amazon->getLabels($event['Records']);
    if (isset($labelsList['message'])) {
        return ['error' => $labelsList['message']];
    }

    foreach ($labelsList as $image => $labels) {
        if (!in_array('Dog', $labels)) {
            $sendResults[] = $amazon->sendEmail($image);
            continue;
        }
        $dbResults[] = $amazon->saveToDB($image, $labels);
    }

    print(json_encode([$dbResults, $sendResults]));
    return [];
};
